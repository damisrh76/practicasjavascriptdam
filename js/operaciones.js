


const btnCalcular= document.getElementById('btnCalcular');
const btnLimpiar = document.getElementById('btnLimpiar');

btnLimpiar.addEventListener('click', limpiar);
btnCalcular.addEventListener('click', function calcular(){

    const Numero1= document.getElementById('idNum1').value;
    const option  = document.getElementById('idOpciones').value;
    const Numero2 = document.getElementById('idNum2').value;
    const txtres = document.getElementById('idResultado');



let res = 0;
let num1 = parseInt(Numero1);
let num2 = parseInt(Numero2);
let opc = parseInt(option);

switch(opc){
     
    case 1 : res= num1+num2 ; break;
    case 2 : res= num1-num2 ; break;
    case 3 : res= num1*num2 ; break;
    case 4 : res= num1/num2 ; break;
}

txtres.value = res;
   
}
);


function limpiar(){
    document.getElementById('idNum1').value ='';
    document.getElementById('idOpciones').value = '';
    document.getElementById('idNum2').value ='';
    document.getElementById('idResultado').value ='';
}

